"use strict";

const request = require("request");
const Promise = require("bluebird");
const url = require("url");

function get(destination, address, method = "GET", form) {
  let parsed = url.parse(address, true);
  delete parsed.host;
  delete parsed.search;

  parsed.hostname = destination.hostname;
  parsed.protocol = destination.protocol;

  let u = url.format(parsed);
  console.error(`# ${method} ${u}`);

  return new Promise(function(resolve, reject) {
    let headers = {};

    if (destination.cookie) headers.Cookie = destination.cookie;
    if (destination.auth) headers.Authorization = `Bearer ${destination.auth}`;

    let requestOptions = {
      method: method,
      url: u,
      json: true,
      gzip: true,
      headers: headers
    };

    if (form) {
      headers["Content-Type"] = "application/json";
      requestOptions.body = form;
    }

    request(requestOptions, function(error, response, body) {
      if (error) return reject(error);
      if (response.statusCode >= 400) {
        return reject(new Error(`HTTP ${response.statusCode}: ${JSON.stringify(body)}`));
      }

      resolve(body);
    });
  });
}

module.exports = get;
